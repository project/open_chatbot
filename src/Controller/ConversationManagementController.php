<?php

namespace Drupal\open_chatbot\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class ConversationManagementController.
 */
class ConversationManagementController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityRepository = $container->get('entity.repository');
    return $instance;
  }

  /**
   * Renderflowchart.
   *
   * @return string
   *   Return Hello string.
   */
  public function renderFlowchart() {

    $data = $this->generateChartDataFromTaxonomy();

    // Get canvas settings.
    $config_pages = \Drupal::service('config_pages.loader');
    $ratio = $config_pages->getValue('chatbot_settings', 'field_canvas_size')[0]['value'];

    if (empty($ratio)) {
      $ratio = 1;
    }

    $width = 8000 * $ratio;
    $height = 4000 * $ratio;

    return [
      '#theme' => 'render_flowchart',
      '#taxonomy' => 'Dynamic value',
      '#var' => [
        'width' => $width,
        'height' => $height,
      ],
      '#loader_path' => \Drupal::service('extension.path.resolver')->getPath('module', 'open_chatbot'),
      '#attached' => [
        'library' => [
          'open_chatbot/flowchart',
        ],
        'drupalSettings' => [
          'open_chatbot' => [
            'data' => $data,
          ],
        ],
      ],
    ];
  }

  /**
   * Generate json data for the plugin.
   */
  public function generateChartDataFromTaxonomy() {
    $operators = $links = [];
    $active_domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    // Condition for load the terms.
    $condition = [
      'vid' => 'chatbot',
      'field_domain' => $active_domain->id(),
    ];
    foreach ($this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($condition) as $term) {
      $sourcelangcode = $term->get('langcode')->getString();
      // Translation.
      $term = $this->entityRepository->getTranslationFromContext($term, $langcode);

      // Prepare inputs.
      $inputs = [];
      foreach ($term->get('field_inputs')->referencedEntities() as $input) {
        $inputs[$input->get('field_id')->getString()] = [
          'label' => $input->get('field_label')->getString(),
        ];
      }

      // Prepare outputs.
      $outputs = [];
      foreach ($term->get('field_outputs')->referencedEntities() as $output) {
        $outputs[$output->get('field_id')->getString()] = [
          'label' => $output->get('field_label')->getString(),
        ];
      }

      // Edit URL.
      $destination = Url::fromRoute('open_chatbot.conversation_management_controll_renderflowchart')->toString();
      if ($term->hasTranslation($langcode)) {
        $url = Url::fromRoute('entity.taxonomy_term.edit_form', ['taxonomy_term' => $term->id()], ['query' => ['destination' => $destination]])->toString();
      }
      else {
        $url = Url::fromRoute('entity.taxonomy_term.content_translation_add', [
          'taxonomy_term' => $term->id(),
          'target' => $langcode,
          'source' => $sourcelangcode,
        ],
        [
          'query' => [
            'destination' => $destination,
          ],
        ]
        )->toString();
      }

      $translation = Url::fromRoute('entity.taxonomy_term.content_translation_overview', ['taxonomy_term' => $term->id()], ['query' => ['destination' => $destination]])->toString();

      // Add option ajax url.
      $addoption = Url::fromRoute('open_chatbot.add.option', ['taxonomy_term' => $term->id()])->toString();

      $html = '';
      if (!$term->get('description')->isEmpty()) {
        $body = $term->get('description')->getValue()[0]['value'];
        $body = strip_tags($body, '<p>');
        $body = substr($body, 0, 100);
        $body = mb_convert_encoding($body, 'UTF-8', 'UTF-8');
        $html = '<div class="trimmed-body">' . $body . '</div><div class="tootip-wrapper"><a href="#" class="body-text">' . $this->t('View Content') . '</a><div class="tooltip">' . $term->get('description')->getValue()[0]['value'] . '</div></div>';
      }
      // If any options are selected then ignore Option Label and Option Body.
      if (!$term->get('field_option_actions')->isEmpty()) {
        $html .= '<div class="action-selected">' . $this->t('Action: ') . $term->get('field_option_actions')->view(['label' => 'hidden'])[0]['#markup'] . '</div>';
      }

      // Prepare operators.
      $operators[$term->field_operator_id->getString()] = [
        'left' => $term->field_position_left->getString(),
        'top' => $term->field_position_top->getString(),
        'properties' => [
          'tid' => $term->id(),
          'title' => "ID : " . $term->id(),
          'inputs' => $inputs,
          'outputs' => $outputs,
          'body' => $html . '<div class="wrapper-button">' . '<div class="operation"><a class="use-ajax" href="' . $url . '" data-dialog-type="dialog" data-dialog-options="{&quot;dialogClass&quot;: &quot;ui-ask-question-modal ui-user-login-modal&quot;}">' . $this->t("Edit") . '</a></div><div class="add-option"><a class="use-ajax" href="' . $addoption . '" >' . $this->t("Add Option") . '</a></div><div class="add-option"><a target="_blank" href="' . $translation . '" >' . $this->t("Translate Overview") . '</a></div></div>',
        ],
      ];
      $operatoridarray[$term->field_operator_id->getString()] = $term->field_operator_id->getString();

      // Prepare links.
      $connectors = $term->get('field_link_connector')->referencedEntities();
      if (!empty($connectors)) {
        foreach ($connectors as $connector) {
          $links[] = [
            'fromOperator' => $connector->get('field_id')->getString(),
            'fromConnector' => $connector->get('field_label')->getString(),
            'toOperator' => $connector->get('field_tooperator')->getString(),
            'toConnector' => $connector->get('field_toconnector')->getString(),
            'color' => '#' . $this->random_color(),
          ];
        }
      }
    }

    // Evaluate links.
    if (!empty($links)) {
      foreach ($links as $key => $link) {
        if (!in_array($link['toOperator'], $operatoridarray)) {
          unset($links[$key]);
        }
      }
    }

    $data = [
      'operators' => $operators,
      'links' => $links,
    ];

    return $data;
  }

  /**
   * Get the initial message.
   *
   * @return json
   *   Json response.
   */
  public function getInitialMessage() {

    if (!\Drupal::request()->isXmlHttpRequest()) {
      // If not an AJAX request, deny access.
      throw new AccessDeniedHttpException();
    }

    // Adds cache tags based on vocabulary.
    $active_domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    $vocabs = ['chatbot'];
    $cacheTags = preg_filter('/^/', 'taxonomy_term_list:', $vocabs);

    // Check the content already cached or not.
    $data = [];
    if ($cache = \Drupal::cache()->get('chatbotconversationinitial_' . $active_domain->id() . '_' . $langcode)) {
      $data = $cache->data;
    }
    else {
      $data = $this->generateConversationData();
      \Drupal::cache()->set('chatbotconversationinitial_' . $active_domain->id() . '_' . $langcode, $data, Cache::PERMANENT, $cacheTags);
    }

    // Add Cache settings for Max-age and URL context.
    // You can use any of Drupal's contexts, tags, and time.
    $cachedependacy['#cache'] = [
      'contexts' => [
        'url',
        'languages:language_content',
      ],
      'tags' => $cacheTags,
    ];
    $response = new CacheableJsonResponse($data, 200);
    $response->addCacheableDependency($cachedependacy);
    return $response;

  }

  /**
   * Add option on specific term.
   *
   * @param \Drupal\taxonomy\Entity\Term $taxonomy_term
   *   Taxonomy term from url.
   *
   * @return json
   *   Json response.
   */
  public function addOption(Term $taxonomy_term) {

    if (!\Drupal::request()->isXmlHttpRequest()) {
      // If not an AJAX request, deny access.
      throw new AccessDeniedHttpException();
    }

    // Add logic for new output property.
    $outputs = $taxonomy_term->get('field_outputs')->referencedEntities();
    if ($count = count($outputs)) {
      // Create new.
      $current = $taxonomy_term->get('field_outputs')->getValue();
      $paragraph = Paragraph::create(['type' => 'chatbot_link']);
      $paragraph->set('field_id', 'option_' . $count);
      $paragraph->set('field_label', 'Option ' . $count + 1);
      $paragraph->isNew();
      $paragraph->save();
      $current[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    else {
      $paragraph = Paragraph::create(['type' => 'chatbot_link']);
      $paragraph->set('field_id', 'option_0');
      $paragraph->set('field_label', 'Option 1');
      $paragraph->isNew();
      $paragraph->save();
      $current[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    $taxonomy_term->set('field_outputs', $current);
    $taxonomy_term->save();
    $response = new AjaxResponse();
    $url = Url::fromRoute('open_chatbot.conversation_management_controll_renderflowchart')->toString();
    $response->addCommand(new RedirectCommand($url));
    return $response;
  }

  /**
   * Get Message by option.
   */
  public function getMessageByOption(Term $taxonomy_term) {

    if (!\Drupal::request()->isXmlHttpRequest()) {
      // If not an AJAX request, deny access.
      throw new AccessDeniedHttpException();
    }

    $active_domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    // Adds cache tags based on vocabulary.
    $vocabs = ['chatbot'];
    $cacheTags = preg_filter('/^/', 'taxonomy_term_list:', $vocabs);
    $cacheTags[] = 'taxonomy_term:' . $taxonomy_term->id();

    // Check the content already cached or not.
    $data = [];
    if ($cache = \Drupal::cache()->get('chatbotconversation_' . $taxonomy_term->id() . '_' . $active_domain->id() . '_' . $langcode)) {
      $data = $cache->data;
    }
    else {
      $data = $this->generateConversationData($taxonomy_term);
      \Drupal::cache()->set('chatbotconversation_' . $taxonomy_term->id() . '_' . $active_domain->id() . '_' . $langcode, $data, Cache::PERMANENT, $cacheTags);
    }

    // Add Cache settings for Max-age and URL context.
    // You can use any of Drupal's contexts, tags, and time.
    $cachedependacy['#cache'] = [
      'contexts' => [
        'url',
        'languages:language_content',
      ],
      'tags' => $cacheTags,
    ];
    $response = new CacheableJsonResponse($data, 200);
    $response->addCacheableDependency($cachedependacy);
    return $response;
  }

  /**
   * Generate conversation data.
   *
   * @param mixed $taxonomy_term
   *   Taxonomy term object or empty.
   *
   * @return array
   *   Conversation data.
   */
  public function generateConversationData($taxonomy_term = NULL) {
    $active_domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    // Condition for load terms.
    $condition = [
      'vid' => 'chatbot',
      'field_domain' => $active_domain->id(),
    ];
    if (empty($taxonomy_term)) {
      // Get initial term logic.
      $taxonomy_term = new \stdClass();
      $parentchild = $operatorterm = [];
      foreach ($this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($condition) as $term) {
        // Logic for the initial term.
        if ($term->get('field_inputs')->isEmpty() && !$term->get('field_outputs')->isEmpty()) {
          $taxonomy_term = $term;
        }
        // Prepare parent child list, this should be store in cache.
        $connectors = $term->get('field_link_connector')->referencedEntities();
        if (!empty($connectors)) {
          foreach ($connectors as $connector) {
            $parentchild[$connector->get('field_id')->getString()][] = $connector->get('field_tooperator')->getString();
          }
        }
        // Prepare operator id and term id array.
        $operatorterm[$term->field_operator_id->getString()] = $term;
      }
    }
    else {
      $parentchild = $operatorterm = [];
      foreach ($this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($condition) as $term) {
        // Prepare parent child list, this should be store in cache.
        $connectors = $term->get('field_link_connector')->referencedEntities();
        if (!empty($connectors)) {
          foreach ($connectors as $connector) {
            $parentchild[$connector->get('field_id')->getString()][] = $connector->get('field_tooperator')->getString();
          }
        }
        // Prepare operator id and term id array.
        $operatorterm[$term->field_operator_id->getString()] = $term;
      }
    }

    $data = [];
    // Throw error response.
    if ($taxonomy_term instanceof Term) {
      // Fetch values from initial item and translation.
      $taxonomy_term = $this->entityRepository->getTranslationFromContext($taxonomy_term, $langcode);
      $message = $taxonomy_term->get('description')->value;

      // Prepare option.
      $operatorid = $taxonomy_term->field_operator_id->getString();

      // Get childs Operator ids.
      $childids = $parentchild[$operatorid] ?? [];

      // Child operator to term object, retriving the option lable and tid.
      $options = [];
      if (!empty($childids)) {
        foreach ($childids as $child) {
          // Check if option action selected.
          if ($operatorterm[$child]->get('field_option_actions')->isEmpty()) {
            $optionterm = $this->entityRepository->getTranslationFromContext($operatorterm[$child], $langcode);
            $options[] = '<div class="options-item"  data-parent="' . $taxonomy_term->id() . '" data-tid="' . $operatorterm[$child]->id() . '">' . $optionterm->get('field_option_label')->value . '</div>';
          }
          else {
            if ($operatorterm[$child]->get('field_option_actions')->getString() == 'restart') {
              $options[] = '<div class="options-item-restart-chatbot" data-parent="' . $taxonomy_term->id() . '" data-tid="' . $operatorterm[$child]->id() . '">' . $this->t("Restart Chatbot") . '</div>';
            }
            if ($operatorterm[$child]->get('field_option_actions')->getString() == 'exit_chatbot') {
              $options[] = '<div class="options-item-exit-chatbot" data-parent="' . $taxonomy_term->id() . '" data-tid="' . $operatorterm[$child]->id() . '">' . $this->t("Exit Chatbot") . '</div>';
            }
          }
        }
      }

      $data = [
        "status" => "success",
        'body' => $message,
        'options' => $options,
        'parentid' => $taxonomy_term->id(),
        "message" => t("Successfully prepared the data"),
      ];
    }

    return $data;
  }

  /**
   *
   */
  public function random_color_part() {
    // Avoid black and white color, so bw 20 and 245.
    return str_pad(dechex(mt_rand(20, 245)), 2, '0', STR_PAD_LEFT);
  }

  /**
   *
   */
  public function random_color() {
    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
  }

}
