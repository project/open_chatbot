<?php

namespace Drupal\open_chatbot\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ChatbotBlock' block.
 *
 * @Block(
 *  id = "chatbot_block",
 *  admin_label = @Translation("Chatbot block"),
 * )
 */
class ChatbotBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\open_custom\Override\ConfigurableLanguageManager definition.
   *
   * @var \Drupal\open_custom\Override\ConfigurableLanguageManager
   */
  protected $languageLanguageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->languageLanguageManager = $container->get('language.language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Retrive settings.
    $config_pages = \Drupal::service('config_pages.loader');
    $fid = $config_pages->getValue('chatbot_settings', 'field_window_icon')[0]['target_id'];
    if (!empty($fid)) {
      $file = File::load($fid);
      $url = \Drupal::service('file_url_generator')->generateString($file->getFileUri());
    }

    $settings = [
      'window_name' => $config_pages->getValue('chatbot_settings', 'field_note_title')[0]['value'],
      'intro_text_desktop' => $config_pages->getValue('chatbot_settings', 'field_intro_text')[0]['value'],
      'intro_text_desktop_word_limit' => $config_pages->getValue('chatbot_settings', 'field_intro_text_desktop_wordlen')[0]['value'],
      'intro_text_mobile' => $config_pages->getValue('chatbot_settings', 'field_intro_text_mobile')[0]['value'],
      'intro_text_mobile_word_limit' => $config_pages->getValue('chatbot_settings', 'field_intro_text_mobile_wordlen')[0]['value'],
      'window_icon' => $url,
    ];

    $build = [];
    $build['#theme'] = 'chatbot_block';
    $build['#content']['settings'] = $settings;
    $build['#attached'] = [
      'library' => [
        'open_chatbot/chatbotwindow',
      ],
      'drupalSettings' => [
        'open_chatbot' => [
          'welcome_message' => $config_pages->getValue('chatbot_settings', 'field_need_help_text')[0]['value'],
        ],
      ],
    ];

    return $build;
  }

}
