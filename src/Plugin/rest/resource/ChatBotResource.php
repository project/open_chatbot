<?php

namespace Drupal\open_chatbot\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to post nodes.
 *
 * @RestResource(
 *   id = "open_chatbot_resource",
 *   label = @Translation("Open Chatbot Resource"),
 *   uri_paths = {
 *     "create" = "/rest/api/post/open-chatbot"
 *   }
 * )
 */
class ChatBotResource extends ResourceBase {

  use StringTranslationTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    $instance->requestStack = $container->get('request_stack')->getCurrentRequest();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Responds to POST requests.
   *
   * Create vocabulary.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post() {
    $data = Json::decode($this->requestStack->getContent(), TRUE);
    $active_domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    if (!empty($data)) {

      // Process links.
      $processedlink = [];
      foreach ($data['links'] as $linkid => $link) {
        $processedlink[$link['fromOperator']][] = $link;
      }

      // Deletion logic.
      // Load existing terms id.
      $existingtids = [];

      // Condition to load terms.
      $condition = [
        'vid' => 'chatbot',
        'field_domain' => $active_domain->id(),
      ];
      foreach ($this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($condition) as $term) {
        $existingtids[$term->field_operator_id->getString()] = $term->field_operator_id->getString();
      }

      // Process operators.
      $processedoperatorids = [];
      foreach ($data['operators'] as $operatorid => $operator) {
        // Prepare current operation ids.
        $processedoperatorids[$operatorid] = $operatorid;
        // Create new operators.
        if (!isset($operator['properties']['tid'])) {
          // Creating new terms for operator.
          $new_term = Term::create([
            'vid' => 'chatbot',
            'name' => $operator['properties']['title'],
            'field_position_top' => $operator['top'],
            'field_position_left' => $operator['left'],
            'field_operator_id' => $operatorid,
            'domain_access' => $active_domain->id(),
            'field_domain' => $active_domain->id(),
          ]);
          $new_term->enforceIsNew();

          // Create input links.
          if (!empty($operator['properties']['inputs'])) {
            // Create paragraph entity for store the inputs value.
            $inputs = [];
            foreach ($operator['properties']['inputs'] as $inputid => $input) {
              $paragraph = Paragraph::create(['type' => 'chatbot_link']);
              $paragraph->set('field_id', $inputid);
              $paragraph->set('field_label', $input['label']);
              $paragraph->isNew();
              $paragraph->save();
              $inputs[] = [
                'target_id' => $paragraph->id(),
                'target_revision_id' => $paragraph->getRevisionId(),
              ];
            }
            $new_term->set('field_inputs', $inputs);
          }

          // Create output links.
          if (!empty($operator['properties']['outputs'])) {
            // Create paragraph entity for store the outputs value.
            $outputs = [];
            foreach ($operator['properties']['outputs'] as $outputid => $output) {
              $paragraph = Paragraph::create(['type' => 'chatbot_link']);
              $paragraph->set('field_id', $outputid);
              $paragraph->set('field_label', $output['label']);
              $paragraph->isNew();
              $paragraph->save();
              $outputs[] = [
                'target_id' => $paragraph->id(),
                'target_revision_id' => $paragraph->getRevisionId(),
              ];
            }
            $new_term->set('field_outputs', $outputs);
          }

          // Update links within the term.
          if (!empty($processedlink)) {
            if (isset($processedlink[$operatorid])) {
              $links = [];
              foreach ($processedlink[$operatorid] as $link) {
                $paragraph = Paragraph::create(['type' => 'chatbot_link_connector'],);
                $paragraph->set('field_label', $link['fromConnector']);
                $paragraph->set('field_id', $link['fromOperator']);
                $paragraph->set('field_toconnector', $link['toConnector']);
                $paragraph->set('field_tooperator', $link['toOperator']);
                $paragraph->isNew();
                $paragraph->save();
                $links[] = [
                  'target_id' => $paragraph->id(),
                  'target_revision_id' => $paragraph->getRevisionId(),
                ];
              }
              $new_term->set('field_link_connector', $links);
            }
          }

          $new_term->save();
        }
        // Update operator.
        else {
          // Load term by tid and only update the link_connector.
          $term = Term::load($operator['properties']['tid']);

          // Position updated.
          $term->set('field_position_top', $operator['top']);
          $term->set('field_position_left', $operator['left']);

          // Update links.
          if (!empty($processedlink)) {
            if (isset($processedlink[$operatorid])) {
              // Unset existing data.
              // @todo Find solution for better value.
              $term->set('field_link_connector', []);
              $links = [];
              foreach ($processedlink[$operatorid] as $link) {
                $paragraph = Paragraph::create(['type' => 'chatbot_link_connector'],);
                $paragraph->set('field_label', $link['fromConnector']);
                $paragraph->set('field_id', $link['fromOperator']);
                $paragraph->set('field_toconnector', $link['toConnector']);
                $paragraph->set('field_tooperator', $link['toOperator']);
                $paragraph->isNew();
                $paragraph->save();
                $links[] = [
                  'target_id' => $paragraph->id(),
                  'target_revision_id' => $paragraph->getRevisionId(),
                ];
              }
              $term->set('field_link_connector', $links);
            }
          }
          else {
            $term->set('field_link_connector', []);
          }
          $term->save();

        }
      }

      // Diff from existing to proccessed operator's id.
      $tobedeleted = array_diff($existingtids, $processedoperatorids);
      if (!empty($tobedeleted)) {

        foreach ($tobedeleted as $key => $value) {
          // Condition to load terms.
          $condition = [
            'field_operator_id' => $key,
            'field_domain' => $active_domain->id(),
          ];
          $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($condition);
          if (!empty($terms)) {
            foreach ($terms as $term) {
              // Delete chatbot_link_connector related to this term..
              $query = $this->entityTypeManager->getStorage('paragraph')->getQuery()
                ->accessCheck(FALSE)
                ->condition('field_tooperator', $term->field_operator_id->getString());
              // Delete chatbot_link_connector paragraph.
              $pids = $query->execute();
              if (!empty($pids)) {
                $pid = reset($pids);
                $pobj = $this->entityTypeManager->getStorage('paragraph')->load($pid);
                $pobj->delete();
              }
              // Delete term.
              $term->delete();
            }
          }
        }
      }
    }

    \Drupal::messenger()->addMessage('Successfully Saved');
    $response = [
      'message' => 'Successfully Saved',
      'action' => 'reload',
    ];
    return new ResourceResponse($response, 200);

  }

}
