
(function ($, Drupal, drupalSettings) {

  $(document).ready(function() {
    var $flowchart = $('#chatbot_canvas');
    var $controles = $('#chat_controles');
    var $container = $flowchart.parent();

    // Panzoom initialization.
    $flowchart.panzoom({ $reset: $controles.find(".reset")});

    // Start with Left Top corner.
    $flowchart.panzoom('pan', 0, 0);

    // Panzoom zoom handling...
    var possibleZooms = [0.5, 0.75, 1, 2, 3];
    var currentZoom = 2;
    // $container.on('mousewheel.focal', function( e ) {
    //     e.preventDefault();
    //     var delta = (e.delta || e.originalEvent.wheelDelta) || e.originalEvent.detail;
    //     var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
    //     currentZoom = Math.max(0, Math.min(possibleZooms.length - 1, (currentZoom + (zoomOut * 2 - 1))));
    //     $flowchart.flowchart('setPositionRatio', possibleZooms[currentZoom]);
    //     $flowchart.panzoom('zoom', possibleZooms[currentZoom], {
    //         animate: false,
    //         focal: e
    //     });
    // });

    // zoom increment.
    $('#zoominbtn1').click(function() {
            currentZoom = Math.max(0, Math.min(possibleZooms.length - 1, (currentZoom + (true * 2 - 1))));
            $flowchart.flowchart('setPositionRatio', possibleZooms[currentZoom]);
            $flowchart.panzoom('zoom', possibleZooms[currentZoom], {
                animate: false,
                focal: {
                  clientX: $container.width() / 2,
                  clientY: $container.height() / 2
                }
            });
    });
  
    // zoom decrement.
    $('#zoomoutbtn1').click(function() {
      currentZoom = Math.max(0, Math.min(possibleZooms.length - 1, (currentZoom + (false * 2 - 1))));
      $flowchart.flowchart('setPositionRatio', possibleZooms[currentZoom]);
      $flowchart.panzoom('zoom', possibleZooms[currentZoom], {
          animate: false,
          focal: {
            clientX: $container.width() / 2,
            clientY: $container.height() / 2
          }
      });
    });



    // Apply the plugin on a standard, empty div...
    $flowchart.flowchart({
      data: drupalSettings.open_chatbot.data,
    });

    // Bind Ajax behaviors.
    Drupal.attachBehaviors();

    $flowchart.parent().siblings('.delete_selected_button').click(function() {
      $flowchart.flowchart('deleteSelected');
    });

    var $draggableOperators = $('.draggable_operator');
    
    function getOperatorData($element) {
      var nbInputs = parseInt($element.data('nb-inputs'));
      var nbOutputs = parseInt($element.data('nb-outputs'));
      var data = {
        properties: {
          title: $element.text(),
          inputs: {},
          outputs: {}
        } 
      };
      
      var i = 0;
      for (i = 0; i < nbInputs; i++) {
        data.properties.inputs['input_' + i] = {
          label: 'Input ' + (i + 1)
        };
      }
      for (i = 0; i < nbOutputs; i++) {
        data.properties.outputs['output_' + i] = {
          label: 'Option ' + (i + 1)
        };
      }
      
      return data;
    }
    
    var operatorId = 0;
        
    $draggableOperators.draggable({
        cursor: "move",
        opacity: 0.7,
        
        helper: 'clone', 
        appendTo: 'body',
        zIndex: 1000,
        
        helper: function(e) {
          var $this = $(this);
          var data = getOperatorData($this);
          return $flowchart.flowchart('getOperatorElement', data);
        },
        stop: function(e, ui) {
            var $this = $(this);
            var elOffset = ui.offset;
            var containerOffset = $container.offset();
            if (elOffset.left > containerOffset.left &&
                elOffset.top > containerOffset.top && 
                elOffset.left < containerOffset.left + $container.width() &&
                elOffset.top < containerOffset.top + $container.height()) {

                var flowchartOffset = $flowchart.offset();

                var relativeLeft = elOffset.left - flowchartOffset.left;
                var relativeTop = elOffset.top - flowchartOffset.top;

                var positionRatio = $flowchart.flowchart('getPositionRatio');
                relativeLeft /= positionRatio;
                relativeTop /= positionRatio;
                
                var data = getOperatorData($this);
                data.left = relativeLeft;
                data.top = relativeTop;
                
                $flowchart.flowchart('addOperator', data);
            }
        }
    });
  
     // Import json data to taxonomy.
    $flowchart.parent().siblings('#save_data').click(function() {
      $.get(Drupal.url('session/token')).done(function (token) {
        var data = $flowchart.flowchart('getData');
        $.ajax({
          method: 'POST',
          url: Drupal.url('rest/api/post/chatbot'),
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-CSRF-Token': token,
          },
          dataType:'json',
          data: JSON.stringify(data),
          beforeSend: function() {
            $("#loader").show();
          },
          success: function(response) {
            console.log(response);
            if (response.action == 'reload') {
              location.reload(true);
            }
          },
          complete:function(data){
            if (data.status == 200)  {
              $("#loader").hide();
            }
            else {
              alert(Drupal.t('Something went wrong !!!'));
            }
          }
        });
     });
    });

    // Generate HTML for draggable custom button.
    $("#generator-form").submit(function(event) {
      event.preventDefault();
  
      const title = $("#title").val();
      const input = parseInt($("#input").val());
      const output = parseInt($("#output").val());

      // Validation required atleast one input or output reqired.
      if (output == 0 && input == 0) {
        $(".error-message").html(Drupal.t('Please enter at least one input or output value.'));
        return false;
      }
      
      const generatedHTML = `<div class="draggable_operator" data-nb-inputs="${input}" data-nb-outputs="${output}">${title}</div>`;
      $("#generated_output").html(generatedHTML);

      var $draggableOperators = $('.draggable_operator');
      $draggableOperators.draggable({
        cursor: "move",
        opacity: 0.7,
        
        helper: 'clone', 
        appendTo: 'body',
        zIndex: 1000,
        
        helper: function(e) {
          var $this = $(this);
          var data = getOperatorData($this);
          return $flowchart.flowchart('getOperatorElement', data);
        },
        stop: function(e, ui) {
            var $this = $(this);
            var elOffset = ui.offset;
            var containerOffset = $container.offset();
            if (elOffset.left > containerOffset.left &&
                elOffset.top > containerOffset.top && 
                elOffset.left < containerOffset.left + $container.width() &&
                elOffset.top < containerOffset.top + $container.height()) {

                var flowchartOffset = $flowchart.offset();

                var relativeLeft = elOffset.left - flowchartOffset.left;
                var relativeTop = elOffset.top - flowchartOffset.top;

                var positionRatio = $flowchart.flowchart('getPositionRatio');
                relativeLeft /= positionRatio;
                relativeTop /= positionRatio;
                
                var data = getOperatorData($this);
                data.left = relativeLeft;
                data.top = relativeTop;
                
                $flowchart.flowchart('addOperator', data);
            }
        }
    });

    });
  });

}) (jQuery, Drupal, drupalSettings);