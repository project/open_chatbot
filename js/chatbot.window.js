(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    const screenWidth = window.innerWidth;
    const chatBotWrapper = document.querySelector(".wrap");
    const btnMobile = document.querySelector(".btn--mobile");
    const chatBotMobileFooter = document.querySelector(
      ".chatbox-footer-mobile"
    );
    const chatBox = document.querySelector(".js-chatbox");
    const chatBoxIntro = document.querySelector(".chatbox-intro");
    const currentPath = window.location.pathname;
    const chatBoxIntroBox = document.querySelector(".chatbox-intro-box");
    const urlPath = window.location.pathname;
    const rtl_languages = ["ar", "ku", "ps", "prs"];
    const isCurrentLanguageRtl = rtl_languages.filter((lang) =>
      urlPath.includes(lang)
    );
    let chatbotMessageCounter = 1;

    if (currentPath.includes("/country-selector")) {
      chatBotWrapper.style.display = "none";
    }

    if (
      localStorage.getItem("chatbotwindowstatus" + drupalSettings.langId) !== null &&
      localStorage.getItem("chatbotwindowstatus" + drupalSettings.langId) === "true" &&
      screenWidth < 500
    ) {
      chatBotWrapper.style.height = "100%";
    }

    btnMobile.addEventListener("click", () => {
      chatBox.classList.toggle("chatbox--is-visible");

      if (chatBox.classList.contains("chatbox--is-visible")) {
        btnMobile.style.display = "none";
        chatBoxIntro.style.display = "none";
        chatBotMobileFooter.style.display = "flex";
        chatBotWrapper.style.background = "#4856DF";
        chatBotWrapper.style.bottom = "0";
        localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, true);
        if (screenWidth < 500) {
          chatBotWrapper.style.height = "100%";
        }
      }
    });

    const minimiseBtn = document.querySelector(".close-btn");
    minimiseBtn.addEventListener("click", () => {
      chatBox.classList.toggle("chatbox--is-visible");
      chatBotMobileFooter.style.display = "none";
      btnMobile.style.display = "flex";
      chatBoxIntro.style.display = "flex";
      chatBotWrapper.style.background = "transparent";
      chatBotWrapper.style.height = "auto";
      if (screenWidth < 500) {
        chatBotWrapper.style.bottom = "7rem";
      } else {
        chatBotWrapper.style.bottom = "1rem";
      }
      localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, false);
    });

    // Chatbot window open close logic.
    const btn = document.querySelector(".js-chat");
    $("#chat-circle").click(function () {
      $("#chat-circle").toggle("scale");
      $(".chat-box").toggle("scale");
    });

    $(".chat-box-toggle").click(function () {
      $("#chat-circle").toggle("scale");
      $(".chat-box").toggle("scale");
    });

    btn.addEventListener("click", () => {
      chatBox.classList.toggle("chatbox--is-visible");

      if (chatBox.classList.contains("chatbox--is-visible")) {
        const chatPopUp = document.querySelector(".wrap");
        // btn.innerHTML = '<i class="fa fa-times"></i>';
        localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, true);
        btn.innerHTML =
          '<div class="chat-bot-button-box chat-bot-button-box-close"></div>';
        chatPopUp.style.alignItems = "flex-end";
        if (screenWidth > 425) {
          chatPopUp.style.gap = "10px";
        }
      } else {
        btn.innerHTML = '<div class="chat-bot-button-box"></div>';
        // btn.innerHTML = '<i class="fa fa-comments"></i>';
        localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, false);
      }
    });

    chatBoxIntroBox.addEventListener("click", () => {
      chatBox.classList.toggle("chatbox--is-visible");

      if (chatBox.classList.contains("chatbox--is-visible")) {
        const chatPopUp = document.querySelector(".wrap");
        // btn.innerHTML = '<i class="fa fa-times"></i>';
        localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, true);
        btn.innerHTML =
          '<div class="chat-bot-button-box chat-bot-button-box-close"></div>';
        chatPopUp.style.alignItems = "flex-end";
        if (screenWidth > 425) {
          chatPopUp.style.gap = "10px";
        }
      } else {
        btn.innerHTML = '<div class="chat-bot-button-box"></div>';
        // btn.innerHTML = '<i class="fa fa-comments"></i>';
        localStorage.setItem("chatbotwindowstatus" + drupalSettings.langId, false);
      }
    })

    // Chatbot window automatically open after 5 second.
    $(document).ready(function() {
      setTimeout(function() {
        if (localStorage.getItem("eventFired" + drupalSettings.langId) === null) {  
          localStorage.setItem("eventFired", true);
          if (!chatBox.classList.contains("chatbox--is-visible") &&  localStorage.getItem('usedflag' + drupalSettings.langId) === null && drupalSettings.path.isFront == true) {
            btn.click();
          }
        }
      }, 5000);
    });

    // Helper function to stop playing all audio files.
    function stopAllAudio() {
      const audioElements = document.querySelectorAll("audio");
      audioElements.forEach(audio => audio.pause());
    }

    // Fetches the audio via the azure TTS service.
    function getVoiceAudio(event) {
      const message = event.target.innerHTML ?? '';
      const messageId = this.attributes[0].value ?? null;
      const messageHtmlDiv = event.target;

      if (message != null && message.length > 0 && messageHtmlDiv?.childNodes?.length == 1) {
        const chat_bot_voice_over_url = 'chatbot/voiceover/ajax';
        $.ajax({
          url: Drupal.url(chat_bot_voice_over_url),
          type: "POST",
          data: JSON.stringify ({ 
            message,
            messageId
          }),
          contentType: "application/json",
          dataType: 'json',
          success: function (response) {
            // Pause all the audio files before playing the new one.
            stopAllAudio();

            // Create the HTML audio tag and play the audio file.
            if (response.audio.status != null && response.audio.status) {
              let audioElement = document.createElement("audio");
              audioElement.setAttribute("controls", "controls");
              audioElement.setAttribute("autoplay", "autoplay");
            
              // Create the source element
              let sourceElement = document.createElement("source");
              sourceElement.setAttribute("src", '/'+response.audio_file_name);
              sourceElement.setAttribute("type", "audio/mp3");
            
              // Add the source element to the audio element
              audioElement.appendChild(sourceElement);
            
              // Create a message for browsers that do not support the audio element
              let errorMessage = document.createTextNode("Your browser does not support the audio element.");
            
              // Create a paragraph element to hold the error message
              let errorParagraph = document.createElement("p");
              errorParagraph.appendChild(errorMessage);
            
              // Append the audio element and error message to the body
              messageHtmlDiv.appendChild(audioElement);
              audioElement.appendChild(errorParagraph);
            }
          },
        });
      }
    }

    // Conversations.
    function sendMessage(message, itsMe, parentMessageId = -1) {
      var messageList = $(".chat-logs");
      var scrollToBottom =
        messageList.prop("scrollHeight") -
          messageList.prop("scrollTop") -
          messageList.prop("clientHeight") <
        80;
      var lastMessage = messageList.children[messageList.children.length - 1];
      var newMessage = document.createElement("div");
      newMessage.innerHTML = message;
      var className;
      if (itsMe == true) {
        className = "me";
        scrollToBottom = true;
      }

      if (itsMe == false) {
        className = "not-me";
        if (parentMessageId != -1) {
          newMessage.setAttribute('chat-bot-message-id', parentMessageId);
          // chatbotMessageCounter += 1;
          newMessage.addEventListener("click", getVoiceAudio);
        }
      }

      if (itsMe == "neutral") {
        className = "divider";
      }

      if (lastMessage && lastMessage.classList.contains(className)) {
        lastMessage.appendChild(document.createElement("br"));
        lastMessage.appendChild(newMessage);
      } else {
        var messageBlock = document.createElement("div");
        messageBlock.classList.add(className);

        if (
          className === "not-me" &&
          isCurrentLanguageRtl != null &&
          isCurrentLanguageRtl.length > 0
        ) {
          messageBlock.classList.add("not-me-rtl");
        }
        messageBlock.appendChild(newMessage);
        messageList.append(messageBlock);
      }

      if (scrollToBottom)
        messageList.scrollTop(messageList.prop("scrollHeight"));

      // Store in localStorage.
      localStorage.setItem("chatbotlogs" + drupalSettings.langId, messageList.html());
    }

    // Ajax GET method function.
    function ajaxGetCallBack(url) {
      $.ajax({
        method: "GET",
        url: Drupal.url(url),
        dataType: "json",
        success: function (response) {
          if (response.length != 0) {
            sendMessage(response.body, false, response.parentid);
            // Line divider.
            if (response.options.length !== 0) {
              var content = Drupal.t("Choose from the option");
              sendMessage(
                '<div data-parent="' +
                  response.parentid +
                  '" class="divider-content"><hr>' +
                  content +
                  "</div>",
                "neutral"
              );
            }
            // Attaching options.
            $.each(response.options, function (index, optionitem) {
              sendMessage(optionitem, true);
            });
          }
        },
      });
    }

    // Check stored conversation are available or not.
    if (localStorage.getItem("chatbotlogs" + drupalSettings.langId) !== null) {
      var messageList = $(".chat-logs");
      messageList.html(localStorage.getItem("chatbotlogs" + drupalSettings.langId));
    } else {
      // Load chatbot welcome message.
      if (drupalSettings.open_chatbot.welcome_message !== "undefined") {
        sendMessage(drupalSettings.open_chatbot.welcome_message, false);
      }
      // Load initial message from API.
      ajaxGetCallBack("chatbot/conversation/initial");
    }

    // Option trigger.
    $(document).on("click", ".options-item", function (event) {
      // Flag to know whether the user used the chatbot.
      localStorage.setItem("usedflag" + drupalSettings.langId, true);

      // Ignore action if option item have clicked class.
      if ($(this).hasClass("clicked")) {
        event.stopPropagation();
        event.preventDefault();
        return false;
      } else {
        var dataTidValue = $(this).data("tid");
        ajaxGetCallBack(
          "chatbot/conversation/message-by-option/" + dataTidValue
        );
        // Disable all options and hide siblings.
        var dataParent = $(this).data("parent");
        // Hide siblings.
        $('[data-parent="' + dataParent + '"]')
          .not($(this))
          .addClass("hide");
        // Added identifier for selected option.
        $(this).addClass("clicked");
        if (isCurrentLanguageRtl != null && isCurrentLanguageRtl.length > 0) {
          $(this).addClass("clicked-rtl");
        }
      }
    });

    // Restart button action.
    function restartAction() {
      // Clear chat logs.
      $(".chat-logs").empty();
      chatbotMessageCounter = 1;

      // Clear the stored HTML content from localStorage
      localStorage.removeItem("chatbotlogs" + drupalSettings.langId);

      // Clear usedflag.
      localStorage.removeItem("usedflag" + drupalSettings.langId);

      // Initialize chat.
      if (drupalSettings.open_chatbot.welcome_message !== "undefined") {
        sendMessage(drupalSettings.open_chatbot.welcome_message, false);
      }
      // Load initial message from API.
      ajaxGetCallBack("open-chatbot/conversation/initial");
    }
    $(document).on("click", ".options-item-restart-chatbot", function () {
      restartAction();
    });
    $(document).on("click", ".restart-chatbot-button", function () {
      restartAction();
    });

    // Restart button action.
    function exitAction() {
      // Clear chat logs.
      chatBotMobileFooter.style.display = "none";
      btnMobile.style.display = "flex";
      chatBoxIntro.style.display = "flex";
      chatBotWrapper.style.background = "transparent";
      chatBotWrapper.style.height = "auto";
      if (screenWidth < 500) {
        chatBotWrapper.style.bottom = "7rem";
      } else {
        chatBotWrapper.style.bottom = "1rem";
      }

      $(".chat-logs").empty();
      // Clear the stored HTML content from localStorage
      localStorage.removeItem("chatbotlogs" + drupalSettings.langId);

      // Initialize chat.
      if (drupalSettings.open_chatbot.welcome_message !== "undefined") {
        sendMessage(drupalSettings.open_chatbot.welcome_message, false);
      }
      // Load initial message from API.
      ajaxGetCallBack("chatbot/conversation/initial");
    }
    $(document).on("click", ".options-item-exit-chatbot", function () {
      exitAction();
      $(".js-chat").trigger("click");
    });

    // Persistant chat window baed on chatbotwindowstatus if the user used the chatwindow.
    if (
      localStorage.getItem("usedflag" + drupalSettings.langId) !== null &&
      localStorage.getItem("usedflag" + drupalSettings.langId) === "true"
    ) {
      $(".js-chat").trigger("click");
      $(".chat-logs").scrollTop($(".chat-logs").prop("scrollHeight"));
      if (screenWidth < 500) {
        chatBotMobileFooter.style.display = "flex";
        chatBotWrapper.style.background = "#4856DF";
        chatBotWrapper.style.bottom = "0";
        btnMobile.style.display = "none";
      }
    }

    if (
      screenWidth > 500 &&
      isCurrentLanguageRtl != null &&
      isCurrentLanguageRtl.length > 0
    ) {
      chatBotWrapper.style.right = "-1em";
    }

    if (isCurrentLanguageRtl != null && isCurrentLanguageRtl.length > 0) {
      const chatLogs = document.querySelector(".chat-logs");
      const elementsWithClassNotMe = chatLogs.getElementsByClassName("not-me");
      if (
        elementsWithClassNotMe != null &&
        elementsWithClassNotMe.classList?.length > 0
      ) {
        elementsWithClassNotMe.classList.add("not-me-rtl");
      } else {
        for (const element of elementsWithClassNotMe) {
          element.classList.add("not-me-rtl");
        }
      }
    }
  });
})(jQuery, Drupal, drupalSettings);
